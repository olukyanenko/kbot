# teleBot

Basic Telegram bot.

## Install
You will find it at [t.me/oluk_kbot_bot](https://t.me/oluk_kbot_bot)

## Commands

### start
- `hello` - Print bot version.

## License
Specify the license under which your project is distributed. For example:

This project is licensed under the MIT License - see the LICENSE file for details.
